<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PageController extends Controller
{
    public function home(Request $request) {
        return view('content.home');
    }

    public function ringkasanEksekutif(Request $request) {
        return view('content.ringkasan-eksekutif');
    }

    public function lingkupLayanan(Request $request) {
        return view('content.lingkup-layanan');
    }

    public function lingkupLayananDua(Request $request) {
        return view('content.lingkup-layanan-2');
    }

    public function lingkupLayananTiga(Request $request) {
        return view('content.lingkup-layanan-3');
    }

    public function praktisiPortofolio(Request $request) {
        return view('content.praktisi-portofolio');
    }

    public function praktisiPortofolioDua(Request $request) {
        return view('content.praktisi-portofolio-2');
    }

    public function praktisiPortofolioTiga(Request $request) {
        return view('content.praktisi-portofolio-3');
    }

    public function praktisiPortofolioEmpat(Request $request) {
        return view('content.praktisi-portofolio-4');
    }

    public function eksklusifitasKlien(Request $request) {
        return view('content.ekslusifitas-klien');
    }

}