<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class LocalizationController extends Controller
{
	/* public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    } */
    //
    public function switchLanguage($locale)
    {
        App::setLocale($locale);

        //storing the locale in session to get it back in the middleware
        session()->put('locale', $locale);
        //dd(App::getLocale());
        return redirect()->back();
    }
}
