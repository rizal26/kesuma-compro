$(window).on("load", function () {
    var url = window.location.pathname.split('/');
    console.log(url);
    var parentUrl = url[1];
    $('#'+parentUrl).addClass('active');
    if (parentUrl == 'lingkup-layanan' || parentUrl == 'lingkup-layanan-2' || parentUrl == 'lingkup-layanan-3') {
        $('#navbarDropdownMenuLink').addClass('active');
    } else if (parentUrl == 'praktisi-portofolio' || parentUrl == 'praktisi-portofolio-2' || parentUrl == 'praktisi-portofolio-3' || parentUrl == 'praktisi-portofolio-4') {
        $('#navbarDropdownMenuLink2').addClass('active');
    } 
});

$(document).ready(function () {
    function handler1() {
        $('#dd-menu').css("display", "block");
        $('#navbarDropdownMenuLink').one("click", handler2);
    }
    
    function handler2() {
        $('#dd-menu').css("display", "none");
        $('#navbarDropdownMenuLink').one("click", handler1);
    }

    $("#navbarDropdownMenuLink").one("click", handler1);

    function portofoliohandler1() {
        $('#dd-menu-2').css("display", "block");
        $('#navbarDropdownMenuLink2').one("click", portofoliohandler2);
    }
    
    function portofoliohandler2() {
        $('#dd-menu-2').css("display", "none");
        $('#navbarDropdownMenuLink2').one("click", portofoliohandler1);
    }
    $("#navbarDropdownMenuLink2").one("click", portofoliohandler1);
});