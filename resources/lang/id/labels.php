<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various menu labels, title labels,
    | and input labels that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    //Menu labels

    'menu-item-groups' => 'Grup',
    'menu-item-komponenbiayas' => 'Komponen Biaya',

    //Input labels
    'input-email' => 'Alamat E-Mail',
    'input-password' => 'Kata Sandi',
    'input-name' => 'Nama',
    'input-confirm-password' => 'Konfirmasi Kata Sandi',
    'input-gender' => 'Jenis Kelamin',
    'input-descr' => 'Keterangan',
    'input-remember-me' => 'Biarkan Tetap Masuk',

    //Input child labels
    'select-item-gender-0' => 'Pilih Jenis Kelamin',
    'select-item-gender-1' => 'Pria',
    'select-item-gender-2' => 'Wanita',

    //Title labels
    'title-login' => 'Masuk',
    'title-register' => 'Registrasi',
    'title-forgot-password' => 'Lupa Kata Sandi?'
    

];