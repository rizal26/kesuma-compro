<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | following language lines contain default error messages used by
    | validator class. Some of these rules have multiple versions such
    | as size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute harus diterimah.',
    'active_url' => ':attribute bukan url yang benar.',
    'after' => ':attribute harus tanggal setelah :date.',
    'after_or_equal' => ':attribute harus tanggal setelah atau sama dengan :date.',
    'alpha' => ':attribute harus berupa huruf.',
    'alpha_dash' => ':attribute harus berupa huruf, nomor, tanda hubung dan garis bawah
.',
    'alpha_num' => ':attribute harus berupa huruf dan angka.',
    'array' => ':attribute harus berupa array.',
    'before' => ':attribute harus tanggal sebelum :date.',
    'before_or_equal' => ':attribute harus tanggal sebelum atau sama dengan :date.',
    'between' => [
        'numeric' => ':attribute harus diantara :min dan :max.',
        'file' => ':attribute harus diantara :min dan :max kilobites.',
        'string' => ':attribute harus diantara :min and :max karakter.',
        'array' => ':attribute harus diantara :min and :max item.',
    ],
    'boolean' => ':attribute field harus true atau false.',
    'confirmed' => ':attribute konfirmasi tidak sama.',
    'date' => ':attribute bukan berupa tanggal.',
    'date_format' => ':attribute tidak sama dengan format :format.',
    'different' => ':attribute dan :other harus berbeda.',
    'digits' => ':attribute harus :digits digit.',
    'digits_between' => ':attribute harus diantara :min dan :max digit.',
    'dimensions' => ':attribute dimensi gambar tidak sesuai.',
    'distinct' => ':attribute duplikat.',
    'email' => ':attribute harus berupa email.',
    'exists' => 'pilihan :attribute tidak sesuai.',
    'file' => ':attribute harus berupa file.',
    'filled' => ':attribute harus diisi.',
    'gt' => [
        'numeric' => ':attribute harus lebih besar dari :value.',
        'file' => ':attribute harus lebih besar dari :value kilobite.',
        'string' => ':attribute harus lebih besar dari :value karakter.',
        'array' => ':attribute harus lebih dari :value item.',
    ],
    'gte' => [
        'numeric' => ':attribute harus lebih besar dari atau sama dengan :value.',
        'file' => ':attribute harus lebih besar dari atau sama dengan :value kilobite.',
        'string' => ':attribute harus lebih besar dari atau sama dengan :value characters.',
        'array' => ':attribute harus sebanyak :value item atau lebih.',
    ],
    'image' => ':attribute harus berupa gambar.',
    'in' => 'pilihan :attribute tidak sesuai.',
    'in_array' => ':attribute tidak ada di :other.',
    'integer' => ':attribute harus berupa bilangan bulat.',
    'ip' => ':attribute harus berupa IP addres yang benar.',
    'ipv4' => ':attribute harus berupa IPv4 address yang benar.',
    'ipv6' => ':attribute harus berupa IPv6 address yang benar.',
    'json' => ':attribute harus berupa JSON string yang benar.',
    'lt' => [
        'numeric' => ':attribute harus lebih kecil dari :value.',
        'file' => ':attribute harus lebih kecil dari :value kilobite.',
        'string' => ':attribute harus lebih kecil dari :value karakter.',
        'array' => ':attribute harus lebih sedikit dari :value item.',
    ],
    'lte' => [
        'numeric' => ':attribute must be less than or equal :value.',
        'file' => ':attribute must be less than or equal :value kilobytes.',
        'string' => ':attribute must be less than or equal :value characters.',
        'array' => ':attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => ':attribute tidak lebih besar dari :max.',
        'file' => ':attribute tidak lebih besar dari :max kilobite.',
        'string' => ':attribute tidak lebih besar dari :max karakter.',
        'array' => ':attribute tidak lebih banyak dari :max item.',
    ],
    'mimes' => ':attribute harus berupa file type: :values.',
    'mimetypes' => ':attribute harus berupa file type: :values.',
    'min' => [
        'numeric' => ':attribute minimal harus :min.',
        'file' => ':attribute minimal harus :min kilobite.',
        'string' => ':attribute minimal harus :min karakter.',
        'array' => ':attribute minimal harus :min item.',
    ],
    'not_in' => 'pilihan :attribute tidak sesuai.',
    'not_regex' => ':attribute format tidak sesuai.',
    'numeric' => ':attribute harus berupa nomor.',
    'present' => ':attribute harus ada.',
    'regex' => ':attribute format tidak sesuai.',
    'required' => ':attribute harus diisi.',
    'required_if' => ':attribute harus diisi ketika :other adalah :value.',
    'required_unless' => ':attribute harus diisi kecuali :other adalah in :values.',
    'required_with' => ':attribute harus diisi ketika :values ada.',
    'required_with_all' => ':attribute harus diisi ketika :values ada.',
    'required_without' => ':attribute harus diisi ketika :values tidak ada.',
    'required_without_all' => ':attribute harus diisi ketika tidak ada :values ada.',
    'same' => ':attribute dan :other harus sama.',
    'size' => [
        'numeric' => ':attribute harus :size.',
        'file' => ':attribute harus :size kilobite.',
        'string' => ':attribute harus :size karakter.',
        'array' => ':attribute harus mengandung :size item.',
    ],
    'string' => ':attribute harus string.',
    'timezone' => ':attribute harus zona yang sesuai.',
    'unique' => ':attribute sudah ada.',
    'uploaded' => ':attribute gagal mengunggah.',
    'url' => ':attribute format tidak sesuai.',
    'uuid' => ':attribute harus berupa UUID yang benar.',
    
    'max_mb' => ':attribute tidak boleh lebih besar dari 2MB.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
