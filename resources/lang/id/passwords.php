<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords minimal 6 karakter dan harus cocok dengan konfirmasi.',
    // 'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Password berhasil dirubah!',
    'sent' => 'Email perubahan password telah dikirim!',
    'token' => 'Token password salah.',
    'user' => "Alamat email tidak ada.",

];
