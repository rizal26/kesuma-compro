@extends('templates.main')
@section('content')
    <style>
        .body-text {
            text-align: justify;
            font-size: 12px;
        }    
        
    </style> 
    <div class="row" style="margin-right: 0; height: 100%">
        <div class="col-lg-9" style="padding-right: 0">
            <div class="masthead" style="padding-top: 5rem; background-color: white; color: white; height: 100%" id="page2" >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
            
                        </div>
                        <div class="col-lg-4" style="z-index: 99">
                            <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                            <span><b>{{__('RINGKASAN EKSEKUTIF')}}</b></span><br><br>
                            <p class="body-text">
                                {{__('Seiring dengan bertumbuh dan berkembangnya keadaan sosial, ekonomi, politik dan teknologi di Indonesia yang kompleks dan penuh dengan dinamika seringkali menimbulkan permasalahan hukum yang beragam sehingga menimbulkan tingginya tuntutan untuk tersedianya layanan profesional asistensi hukum yang berkualitas dan mampu berkomitmen untuk memberikan pelayanan terbaik.')}}
                            </p>
                            <p class="body-text">
                                {{__('Kantor hukum Kesuma Partners hadir untuk menawarkan anda solusi dalam memecahkan setiap permasalahan hukum yang anda alami dengan dedikasi yang kuat serta pendirian yang kokoh bahwa setiap klien kami sangatlah berharga untuk diperjuangkan, oleh karenanya kami selalu menempatkan setiap klien kami sebagai prioritas utama.')}} 
                            </p>
                            <p class="body-text">
                                {{__('Kami sangat menyadari bahwa pelayanan hukum yang baik tidak dapat diwujudkan dengan hanya memiliki pemahaman di bidang hukum saja, oleh karenanya, Kesuma Partners beranggotakan para praktisi hukum yang memiliki pemahaman, pengalaman dan kompetensi secara menyeluruh dan mendalam akan praktik di dunia bisnis, komersial, teknologi, serta hal-hal esensial lainnya yang menjadi pertimbangan utama bagi para pelaku usaha di berbagai sektor industri, terutama sehubungan dengan aktivitas-aktivitas bisnis dalam menjalankan dan mengamankan kegiatan usaha dari klien kami. Sehingga klien kami dapat merasakan dampak yang signifikan baik dari segi pengelolaan perusahaan yang baik (good corporate governance) ataupun terbantu dari segi pengambilan keputusan yang tepat demi menunjang kesuksesannya.')}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3" style="padding-top: 5rem;" id="page-text2">
            <div class="container">
                <div class="col-lg-12">
                    <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                    <span><b>{{__('NILAI UTAMA')}}</b></span><br><br>
                    <p class="body-text">
                        {{__('Cara kami dalam menjaga kualitas dari produk-produk hukum yang kami hasilkan bukan semata-mata hanya karena pengalaman-pengalaman kami di masa terdahulu, akan tetapi kuatnya dedikasi kami untuk selalu melakukan riset secara terus-menerus serta senantiasa berupaya memperbaharui pengetahuan kami akan peraturan perundang-undangan, pelatihan-pelatihan, serta eksplorasi mendalam dari segi hard skill maupun soft skill di bidang hukum, sehingga membuat kami mampu untuk mencapai ekspektasi klien-klien kami.')}}
                    </p>
                    <p class="body-text">
                        {{__('Dengan berbekalkan ilmu dari segi teori dan pengalaman dalam berpraktik yang menyeluruh di berbagai sektor industri, kami percaya bahwa kami memiliki bekal yang cukup dalam menunjang kebutuhan setiap klien kami dalam pengambilan keputusan ataupun tindakan yang tepat. Karena dalam melakukan transaksi, pengambilan keputusan ataupun tindakan yang tepat merupakan kunci utama dalam meraih kesuksesan dalam berbisnis. Anjuran yang berlandaskan analisa yang komprehensif serta basis keilmuan yang mendalam, dapat mengoptimalisasi kebutuhan klien kami serta menghindarkan klien kami dari setiap resiko yang mungkin sulit untuk dideteksi.')}}
                    </p>
                    <p class="body-text">
                        {{__('Kepercayaan klien terhadap integritas dan dedikasi kami adalah pondasi utama dalam membina hubungan kerja sama yang efektif, sehingga klien kami tidak sebatas hanya memperlakukan kami sebagai konsultan hukum, akan tetapi juga sebagai rekan kerja yang dapat dipercaya.')}}
                    </p>
                </div>
            </div>
        </div>
    </div>

@endsection 

