@extends('templates.main')
@section('content')
        <style>
        .body-text {
            text-align: justify;
            font-size: 9.8px;
            font-weight: 700
        }  
        .title-text {
            color: #b45f06;
            font-size: 10.8px
        }  
        ul {
            padding-left: 11px;
        }
    </style>  
    <div class="row" style="margin-right: 0;"> 
        <div class="col-lg-12" style="padding-right: 0">
                <div class="masthead" id="page5-6" style="padding-top: 6.5rem">
                    <div class="container" style="color: white;">
                        <div class="col-lg-12" id="page3-title">
                            <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                            <span style="font-size: 20px"><b>{{__('PRAKTISI & PORTOFOLIO TIM')}}</b></span><br><br>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="row" style="margin-right: 0; padding: 25px;">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/Muhamad_Normansyah.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                            <span class="title-text"><b>Muhamad Normansyah,S.H. - <i>MANAGING PARTNER</i></b></span><br>
                            {{__('Dengan menyandang gelar sarjana hukum dari Universitas Padjadjaran dan mengambil gelar master hukum bisnis di Universitas Indonesia, hal tersebut memberikan pemahaman yang sangat mendalam terhadap teori dan praktik hukum serta implementasinya di lapangan sehingga dapat dipercaya untuk menangani berbagai proyek-proyek besar diantaranya proyek kerjasama antara PT MNC Land Tbk. dengan The Trump Organization, penyelesaian perkara ketenagakerjaan PT Perusahaan Gas Negara dan penyusunan kajian hukum proyek Light Rail Transit Pemerintah Kota Bandung.')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <p class="title-text text-black"><b>{{__('Pengalaman:')}}</b></p>
                    <div class="body-text">
                        <p>
                            <ul>
                                <li>
                                    {{__('Asistensi terhadap PT MNC Land Tbk dalam pengurusan aspek legal dalam kerjasama pengadaan Taman Hiburan Lido Resort antara dengan The Trump Organization, kerjasama pengadaan proyek Infrastruktur real estate, mengevaluasi bisnis atas sengketa tanah, dokumentasi, Leasing, desain mekanik, proyek kelistrikan dan perpipaan;')}}
                                </li>
                                <li>
                                    {{__('Asistensi terhadap PT Nodeflux Teknologi Indonesia dalam mempersiapkan term of services atas produk perusahaan, strategi pemasaran, instrumen distribusi, product pricing, product system topology, product disclaimer, product privacy policy, go to market strategy, perolehan Hak Kekayaan Intelektual, pendirian asosiasi, serta membangun hubungan dengan pihak pemerintahan.')}}
                                </li>
                                <li>
                                    {{__('Asistensi terhadap lembaga keuangan bank dan non-bank dalam melakukan kerjasama perolehan hak akses data dari Direktorat Jenderal Kependudukan dan Pencatatan Sipil Republik Indonesia (DUKCAPIL);')}}
                                </li>
                                <li>
                                    {{__('Kajian hukum tata ruang dan peraturan perundang-undangan dalam penerapan Light Rail Transit Pemerintah Kota Bandung.')}}
                                </li>
                                <li>
                                    {{__('Kajian hukum tata ruang dan peraturan perundang-undangan dalam penerapan Microcell Pole Pemerintah Kota Bandung.')}}
                                </li>
                                <li>
                                    {{__('Asistensi terhadap PT Permata Karya Jasa (“Anak Perusahaan PGN”) dalam manajemen risiko perusahaan, perizinan dan kepatuhan hukum, legalitas perusahaan, perselisihan ketenagakerjaan serta penyelesaian perkara tindak pidana.')}}
                                </li>
                                <li>
                                    {{__('Asistensi terhadap PT Perusahaan Pelayaran Bhaita dalam penyelesaian perkara PKPU.')}}
                                </li>
                                <li>
                                    {{__('Asistensi terhadap PT FWD Life Indonesia dalam penyelesaian perkara poaching dan twisting agen asuransi.')}}
                                </li>
                                <li>
                                    {{__('Asistensi PT AXA Mandiri Financial Services dalam penyelesaian perkara asuransi nasabah terhadap legalitas transaksi telemarketing.')}}
                                </li>
                                <li>
                                    {{__('Asistensi terhadap PT Pertamina Patra Niaga dalam penyelesaian perkara Penundaan Kewajiban Pembayaran Utang.')}}
                                </li>
                                <li>
                                    {{__('Asistensi PT Perusahaan Gas Negara dalam penyelesaian perkara ketenagakerjaan dengan pegawai outsourcing.')}}
                                </li>
                                <li>
                                    {{__('Asistensi dalam pengurusan perizinan perbankan dalam memperoleh izin menerbitkan kartu debit dan kredit;')}}
                                </li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/Tania_Irwan.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                        <span class="title-text"><b>Tania Irwan, S.H. - <i>PARTNER</i></b></span><br>
                        {{__('Dengan mengawali karir sebagai pengacara di salah satu kantor hukum ternama serta dipercaya untuk menjadi konsultan hukum di perusahaan bertaraf internasional, memberikan Tania pemahaman yang luas terhadap praktik korporasi lokal dan asing. Dengan berbagai variasi pekerjaan yang dijalani, memberikan dia kecakapan di berbagai bidang yang diantaranya meliputi pengurusan perusahaan asing, pengurusan Hak Kekayaan Intelektual, korupsi, pencucian uang, perkapalan hingga pengurusan pendirian perusahaan rintisan.')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <p class="title-text text-black"><b>{{__('Pengalaman:')}}</b></p>
                    <div class="body-text">
                        <p>
                            <ul>
                                <li>{{__('Uji tuntas perizinan dan kepemilikan aset dan asistensi dalam pengurusan penyelesaian pembayaran biaya carter kapal pada PT Momentum Indonesia Investama')}}</li>
                                <li>{{__('Asistensi dalam pengurusan perkara korporasi pertanggungjawaban freight forwarder atas tenggelamnya kapal LCT Ayu')}}</li>
                                <li>{{__('Asistensi dalam pengurusan perkara perdata atas tanah PT Modernland Realty Tbk')}}</li>
                                <li>{{__('Asistensi dalam pengurusan aspek legal dalam pendirian PMA dan pengurusan perizinan PT Indomobi Aplikasi Teknologi')}}</li>
                                <li>{{__('Asistensi dalam pendampingan pendaftaran Hak Kekayaan Intelektual Mr DIY')}}</li>
                                <li>{{__('Asistensi dalam pendampingan pemeriksaan saksi dugaan tindak pidana korupsi dan pencucian uang PERUM Damri ')}}</li>
                                <li>{{__('Asistensi dalam pendampingan pemeriksaan saksi dugaan tindak pidana korupsi dan pencucian uang Waskita Karya')}}</li>
                                <li>{{__('Asistensi dalam pendampingan laporan dugaan tindak pidana penipuan dan/atau penggelapan PT Sinarmas Global Mandiri')}}</li>
                                <li>{{__('Asistensi dalam pendampingan pemeriksaan saksi dugaan tindak pidana korupsi dan pencucian uang dalam pengadaan Bus BRT')}}</li>
                                <li>{{__('Asistensi dalam pemeriksaan dugaan tindak pidana lingkungan hidup terkait izin dan penyimpanan Limbah B3')}}</li>
                                <li>{{__('Asistensi dalam pemeriksaan saksi dugaan pelanggaran Overload Overdimensi pada Dinas Perhubungan')}}</li>
                                <li>{{__('Asistensi dalam perkara korporasi terkait gugatan ganti kerugian yang diakibatkan penggelapan oleh kepala cabang Bank Tabungan Negara')}}</li>
                                <li>{{__('Uji tuntas aspek hukum dalam pengadaan kendaraan oleh PT Pindad Persero')}}</li>
                                <li>{{__('Asistensi terhadap perusahaan rintisan di bidang Financial Technology dalam mempersiapkan terms and condition, privacy policy, perizinan, perolehan Hak Kekayaan Intelektual, serta membangun hubungan dengan pihak Asosiasi ')}}</li>
                                <li>{{__('Asistensi dalam pengurusan perjanjian dan sistem kerjasama perusahaan Financial Technology dengan pialang berjangka terdaftar di Bappepti')}}</li>
                                <li>{{__('Asistensi terhadap kepatuhan Dana Pensiun terkait laporan aktuaris kepada Otoritas Jasa Keuangan.')}}</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/Lahiwadifra.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                            <span class="title-text"><b>Lahiwadifra, S.H. - <i>PARTNER</i></b></span><br>
                            {{__('Lahiwadifra adalah seorang kurator handal yang telah berpengalaman menangani dan menyelesaikan puluhan perkara PKPU dan Kepailitan di Indonesia, Pengetahuannya yang luas tentang hukum Kepailitan dari segi teori dan praktik membuatnya menjadi seorang ahli yang mampu menangani setiap tugas dengan sempurna. Tidak dapat diragukan lagi bahwa lahiwadifra adalah seorang pengacara yang terampil, cerdas dan pekerja keras serta memiliki konsistensi dalam menjaga kualitas pekerjaannya bahkan pada saat menangani kasus terberat sekalipun dengan mudahnya. ')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <p class="title-text text-black"><b>{{__('Pengalaman:')}}</b></p>
                    <div class="body-text">
                        <p>
                            <ul style="line-height: 2;">
                                <li>{{__('Berlaku sebagai Kurator dalam perkara kepailitan dari PT Aspalindo Sejahtera Mandiri.')}}</li>
                                <li>{{__('Berlaku sebagai Kurator dalam penyelesaian perkara kepailitan dari PT Nusuno Karya ')}}</li>
                                <li>{{__('Berlaku sebagai Kurator dalam penyelesaian perkara kepailitan dari PT Meriana Khoe ')}}</li>
                                <li>{{__('Asistensi selaku konsultan hukum dalam perkara Penundaan Kewajiban Pembayaran Utang dan Kepailitan PT Bank Mutiara Tbk.')}}</li>
                                <li>{{__('Asistensi terhadap PT Atlas Resource Tbk GROUP dalam perkara Penundaan Kewajiban Pembayaran Utang dan Kepailitan.')}}</li>
                                <li>{{__('Asistensi terhadap PT Bumi Sejahtera Ariya dalam perkara Penundaan Kewajiban Pembayaran Utang dan Kepailitan.')}}</li>
                                <li>{{__('Asistensi terhadap PT Jtrust Investment dalam perkara Kepailitan PT Suharli Malaya Lestari.')}}</li>
                                <li>{{__('Berlaku sebagai Pemohon Intervensi atas perkara The Hongkong and Shanghai Banking Corporation, Kementerian Keuangan RI Cq Direktorat Jenderal Kekayaan Negara Kementerian Keuangan RI Cq Kantor Wilayah DJKN Provinsi Dki Jakarta Cq Kantor Pelayanan Kekayaan Negara Dan Lelang Jakarta, PT. Balai Mandiri Prasarana (Balai Lelang Mandiri), Kementerian Agraria Dan Tata Ruang/Badan Pertanahan Nasional RI Cq Kantor Wilayah Badan Pertanahan Nasional Provinsi DKI Jakarta Cq Kantor Pertanahan Kota Administratif Jakarta Selatan')}}</li>
                                <li>{{__('Asistensi Asistensi dalam pengurusan perkara asuransi PT Axa Mandiri Financial Services.')}}</li>
                                <li>{{__('Asistensi dalam penyelesaian perkara praktik poaching dan twisting pada perusahaan asuransi.')}}</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection 

