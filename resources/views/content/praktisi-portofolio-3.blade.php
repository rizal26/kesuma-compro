@extends('templates.main')
@section('content')
        <style>
        .body-text {
            text-align: justify;
            font-size: 9.8px;
            font-weight: 700
        }  
        .title-text {
            color: #b45f06;
            font-size: 10.8px
        }  
        ul {
            padding-left: 11px;
        }
    </style>  
    <div class="row" style="margin-right: 0;"> 
        <div class="col-lg-12" style="padding-right: 0">
                <div class="masthead" id="page5-6" style="padding-top: 6.5rem">
                    <div class="container" style="color: white;">
                        <div class="col-lg-12" id="page3-title">
                            <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                            <span style="font-size: 20px"><b>{{__('PRAKTISI & PORTOFOLIO TIM')}}</b></span><br><br>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="row" style="margin-right: 0; padding: 25px;">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/Herbert_Nababan.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                            <span class="title-text"><b>Herbert Nababan, S.T., S.H., M.H. - <i>SENIOR ADVISOR</i></b></span><br>
                            <p style="line-height: 2;">{{__('Herbert mengawali karirnya di KPK sebagai Fungsional Spesialis Pencegahan Korupsi pada Kedeputian Pencegahan selama 5 tahun pertama serta telah banyak berkecimpung dalam program-program pencegahan korupsi pada instansi-instansi baik instansi pemerintah (Kementerian/Lembaga dan BUMN/BUMD) maupun instansi Swasta berupa program pendidikan antikorupsi, pengadaan barang dan jasa, gratifikasi dan suap, Laporan Harta Penyelenggara Negara (LHKPN), Compliance Handling, Tindak Pidana Pencucian Uang, Tindak Pidana Korporasi, dll. REKANAN AHLI & PENASIHAT Selanjutnya Herbert pernah menjabat sebagai Spesialis Kerjasama Nasional KPK yang banyak menangani Kerjasama (MoU) antara KPK dengan lembaga-lembaga baik instansi pemerintah maupun swasta dalam negeri maupun luar negeri sehubungan dengan upaya pemberantasan korupsi di Indonesia.10 tahun terakhir (2011-2021), Herbert Nababan dipromosikan sebagai Penyidik KPK yang telah banyak memiliki jaringan dan pengalaman yang luas dalam penyelidikan dan penyidikan perkara-perkara tindak pidana korupsi, tindak pidana pencucian uang serta tindak pidana korporasi di Indonesia yang melibatkan unsur eksekutif, legislatif dan yudikatif, sehingga memberikan Herbert Nababan pengalaman dan pengetahuan yang luar biasa dalam menyelesaikan banyak perkara korupsi, tindak pidana suap, gratifikasi, korupsi dalam pengadaan barang dan jasa, korupsi dalam proyek pertambangan, kehutanan dan sumber daya alam, konflik kepentingan, tindak pidana pencucian uang, tindak pidana korporasi, hingga akhirnya menjabat sebagai Penyidik Senior (senior investigator) KPK dan sebagai pelaksana tugas kepala satuan tugas penyidikan KPK. ')}} 
                            </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="body-text">
                        <p>
                            <ul style="line-height: 2;">
                                <li>{{__('Big Data Analytic untuk Audit Internal, The Institute of Internal Auditors Indonesia (IIA) - Agustus 2020 — Agustus 2020')}}</li>
                                <li>{{__('Akuntansi Forensik (Basic & Advance), Komisi Pemberantasan Korupsi (KPK) dan Ernst & Young - Oktober 2019 — November 2019')}}</li>
                                <li>{{__('Peningkatan Kapasitas Penyidik Anti Korupsi, Korean International Cooperation Agency (KOICA), Korea Selatan - Agustus 2017 — September 2017')}}</li>
                                <li>{{__('Pelatihan Investigasi, Corrupt Practices Investigation Bureau (CPIB), Singapura - Agustus 2014 — Desember 2014')}}</li>
                                <li>{{__('Pertemuan Tahunan Asia Pacific Group ke-15 2012 Brisbane, Australia, Asian Pacific Group on Money Laundering (APG) - Juli 2012')}}</li>
                                <li>{{__('Konferensi Negara Pihak Konvensi Perserikatan Bangsa-Bangsa Menentang Korupsi, Wina, Austria, Perserikatan Bangsa-Bangsa (PBB) - Mei 2011 — Juni 2011')}}</li>
                                <li>{{__('Lokakarya Analisis Tingkat Lanjut Regional, Jaringan Penegakan Kejahatan Keuangan, Wina, Virginia, AS - Maret 2011 — April 2011')}}</li>
                                <li>{{__('Pelatihan Regional untuk Focal Point dan Pakar Pemerintah yang berpartisipasi dalam Mekanisme Tinjauan untuk Perserikatan Bangsa-Bangsa Melawan Korupsi, United Nations Office on Drugs and Crime (UNODC) - Oktober 2010')}}</li>
                                <li>{{__('Lokakarya Tipologi Asia Pacific Group, Dhaka, Bangladesh, Diselenggarakan oleh Grup Asia Pasifik tentang Pencucian Uang - Oktober 2010')}}</li>
                                <li>{{__('Fundamentals of Wealth Management (WM01), Program Sertifikasi Wealth Management, Jakarta - November 2009')}}</li>
                                <li>{{__('Pertemuan Tahunan Asia Pacific Group ke-12 2009, Brisbane, Australia, Grup Asia Pasifik tentang Pencucian Uang - Juli 2009')}}</li>
                                <li>{{__('Lokakarya Peningkatan Kapasitas Pemberantasan Korupsi Terkait Pencucian Uang, Bangkok, Thailand, APEC - Agustus 2007')}}</li>
                                <li>{{__('Pelatihan Pelacakan Aset, Pemulihan dan Pemulangan, Jakarta, Pusat Internasional untuk Pemulihan Aset dan GTZ - September 2007')}}</li>
                                <li>{{__('Pertemuan Tahunan Asia Pacific Group ke-10 2007, Perth, Australia, Grup Asia Pasifik tentang Pencucian Uang - Juli 2007')}}</li>
                                <li>{{__('Workshop Tipologi APG & Pertemuan Pleno Khusus APG, Asia Pacific Group (APG) - November 2006')}}</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-right: 0; padding: 25px; padding-top: 0 !important;">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/Altira_Imanuel_Prasetyo.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                            <span class="title-text"><b>Altira Imanuel Prasetyo, S.H. - <i>SENIOR ADVISOR</i></b></span><br>
                            <p style="line-height: 2;">{{__('Altira mendapatkan gelar sarjana hukum dari Fakultas Hukum Universitas Padjadjaran. Altira mengawali karir di dunia hukum dengan bekerja pada satu kantor advokat di Jakarta yang berfokus kepada penanganan perkara-perkara di bidang pertanahan baik di pulau jawa maupun di luar pulau jawa. Setelah itu Altira kemudian pindah ke salah satu lawfirm di jakarta yang memiliki klien-klien asing dan bekerja pada cooperate and labor section dimana pada divisi ini dikhususkan untuk menangani dan memberikan nasihat maupun penanganan hukum terhadap client khususnya di bidang ketenagakerjaan serta memberikan nasihat hukum terkait pendirian perusahaan dan penjalanan perusahaan. Altira juga merupakan seorang Pengurus dan Kurator yang telah berpengalaman menangani dan menyelesaikan puluhan perkara PKPU dan Kepailitan di Indonesia.')}} 
                            </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="body-text">
                        <p>
                            <ul style="line-height: 2;">
                                <li>{{__('Asistensi perkara ketenagakerjaan di PHI terhadap beberapa perusahaan asal Jepang di Indonesia')}}</li>
                                <li>{{__('Asistensi terhadap pembuatan Peraturan Perusahaan serta Peraturan Kerja Bersama terhadap beberapa perusahaan Jepang di Indonesia')}}</li>
                                <li>{{__('Asistensi terhadap pembuatan skema bisnis salah satu perusahaan asing di Indonesia')}}</li>
                                <li>{{__('Asistensi terhadap salah satu perusahaan manufaktur di Indonesia dengan memberi opini-opini hukum terhadap produk yang akan mereka keluarkan')}}</li>
                                <li>{{__('Asistensi terhadap salah satu perusahaan Financing dalam hal restrukturisasi hutang')}}</li>
                                <li>{{__('Bekerja sama dengan salah satu Perusahaan Financing dengan memberikan seminar mengenai berbagai jenis hukum jaminan di Indonesia')}}</li>
                                <li>{{__('Asistensi terhadap direktur salah satu perusahaan air mineral di Sumatera Selatan dalam permasalahan ultra vires')}}</li>
                                <li>{{__('Asistensi terhadap karyawan dan direktur perusahaan air mineral di Sumatera Selatan dalam kasus hukum di Kepolisian dan Kejaksaan')}}</li>
                                <li>{{__('Asistensi terhadap Rapat Umum Pemegang Saham (RUPS) pada beberapa perusahaan di Indonesia')}}</li>
                                <li>{{__('Asistensi terhadap pengajuan judicial review terhadap hak waris terhadap anak dari pasangan kawin campur')}}</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection 

