@extends('templates.main')
@section('content')
        <style>
        .body-text {
            text-align: justify;
            font-size: 10px;
        }  
        .title-text {
            color: #b45f06;
            font-size: 14px;
        }  
    </style>  
    <div class="row" style="margin-right: 0; height: 100%"> 
        <div class="col-lg-3" style="padding-right: 0">
            <div class="masthead" style="padding-top: 5rem; background-color: white; color: white; height: 100%" id="page4" >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="position: fixed" id="page4-title">
                            <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                            <span><b>{{__('LINGKUP LAYANAN')}}</b></span><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9" style="background-color: #434343;">
            <div class="row" style="padding-top: 5rem; color: white;" id="page4-text">
                {{-- <div class="col-lg-3">
                </div> --}}
                <div class="col-sm-4">
                    <div><span class="title-text">{{__('Korupsi')}}</span></div>
                    <p class="body-text">
                        {{__('Pada perkara Tindak Pidana Korupsi tim kami akan membantu anda baik dalam pendampingan di tingkat Kepolisian, Kejaksaan, KPK dan melakukan pembelaan di tingkat pengadilan pidana tipikor baik berupa mengajukan penangguhan penahanan, eksepsi dakwaan, mengumpulkan alat bukti, mengajukan saksi ahli dan saksi yang meringankan, mengajukan nota pembelaan (Pledoi) dan melakukan upaya hukum lain nya baik berupa banding, kasasi maupun Peninjauan Kembali (PK) di berbagai tingkat Pengadilan. ')}}
                    </p>
                    <div><span class="title-text">{{__('Penggelapan dan Penipuan')}}</span></div>
                    <p class="body-text">
                        {{__('Para praktisi hukum kami telah menuntaskan berbagai perkara dalam bidang Penggelapan dan Penipuan baik dari Pihak Korban atau yang dirugikan untuk membuat laporan di Kepolisian, Asistensi sampai dalam tahap Pelimpahan Kejaksaan, Persidangan sampai dengan Putusan Inkrah, selain itu kami juga dapat berdiri di pihak tersangka atau diduga tersangka dalam melakukan mediasi untuk di selesaikan di luar pengadilan, mengajukan restorative justice dan/atau pendampingan di tingkat Kepolisian serta Pembelaan di tingkat Pengadilan, karena tugas kami yaitu membela berdasarkan hak - hak dari tersangka memperjelas apa yang dituntut, dan menguak di dalam fakta persidangan apakah klien tersebut melakukan secara dipaksa, diancam, atau dilakukan secara bersama-sama yang mana pada praktiknya masih banyaknya dalam kejahatan penggelapan dan penipuan yang dilakukan secara berkelompok sehingga terhindar dari terjadinya kesalahan dalam penetapan tersangka. ')}}
                    </p>
                    <div><span class="title-text">{{__('Pencabulan dan Asusila')}}</span></div>
                    <p class="body-text">
                        {{__('Para praktisi hukum kami telah menuntaskan berbagai perkara dalam bidang pencabulan dan asusila baik tersangka atau diduga tersangka dalam melakukan pendampingan di tingkat Kepolisian dan Pembelaan di tingkat pengadilan, karena tugas kami membela klien berdasarkan hak - hak klien, memperjelas apa yang disangkakan, dan menguak di dalam fakta persidangan apakah klien tersebut memang terdakwa atau hanyalah korban dari kejahatan orang luar yang membuat dia terpaksa melakukan kejahatan tersebut, menemukan fakta hukum baru dalam pembelaan kepada hakim dalam menetapkan putusan / vonis terhadap klien ')}}
                    </p>
                    <div><span class="title-text">{{__('Pencemaran Nama Baik dan Perbuatan Tidak Menyenangkan')}}</span></div>
                    <p class="body-text">
                        {{__('Perlu dipahami bahwa terdapat perbedaan antara pencemaran nama baik dan perbuatan tidak menyenangkan. Ini karena masyarakat terkadang tidak menyadari bahwa kedua tindakan tersebut memiliki landasan hukum berbeda. Para praktisi hukum kami telah berpengalaman dalam menangani kasus Pencemaran Nama Baik dan Perbuatan Tidak Menyenangkan baik dari pihak korban maupun pihak yang diduga tersangka karena di mata hukum perbedaan antara pencemaran nama baik dan perbuatan tidak menyenangkan menentukan berat atau tidaknya hukuman dilayangkan.')}}
                    </p>
                </div>
                <div class="col-sm-4">
                    <div><span class="title-text">{{__('Narkotika dan Obat-obatan Terlarang')}}</span></div>
                    <p class="body-text">
                        {{__('Kami sangat memahami bahwa dalam praktiknya masih banyaknya kasus dimana kepolisian maupun kejaksaan melakukan kesalahan dalam menerapkan pasal dalam tuntutan yang membuat kekeliruan dalam hukum yang mana bisa membuat terpidana salah dalam vonis hukuman oleh Pengadilan. Para praktisi kami telah menuntaskan puluhan perkara dalam bidang Narkotika dan Obat Psikotropika baik tersangka atau diduga tersangka dalam melakukan pendampingan di tingkat Kepolisian dan Pembelaan di tingkat pengadilan baik dalam kasus klien yang menyalahgunakan, menyimpan maupun menjual/memproduksi dengan melakukan upaya hukum yang sesuai dengan UU Narkotika.')}}
                    </p>
                    <div><span class="title-text">{{__('Pencucian Uang')}}</span></div>
                    <p class="body-text">
                        {{__('Para praktisi hukum kami telah menuntaskan berbagai perkara dalam bidang Pencucian Uang baik dari sisi Korban, Tersangka atau diduga tersangka dalam melakukan pendampingan di tingkat Kepolisian dan Pembelaan di tingkat pengadilan, karena tugas kami membela klien berdasarkan hak - hak klien, memperjelas apa yang disangkakan, dan menguak di dalam fakta persidangan apakah klien tersebut memang terdakwa atau hanyalah korban dari kejahatan Perusahaan/kelompok mana pada praktiknya masih banyaknya dalam kejahatan pencucian uang yang dilakukan secara berkelompok tetapi hanya satu orang yang diadili dalam arti lain dijadikan tumbal juga salah orang dalam penetapan tersangka karena adanya situasi dan kondisi yang menyebabkan klien tersebut ditetapkan menjadi tersangka. ')}}
                    </p>
                    <div><span class="title-text">{{__('Cybercrime')}}</span></div>
                    <p class="body-text">
                        {{__('Kejahatan Cybercrime adalah segala aktivitas ilegal yang digunakan oleh pelaku kejahatan dengan menggunakan teknologi sistem informasi jaringan komputer yang secara langsung menyerang teknologi sistem informasi dari korban. Namun secara lebih luas kejahatan cyber bisa juga diartikan sebagai segala tindak ilegal yang didukung dengan teknologi komputer. Praktisi hukum kami telah menuntaskan berbagai perkara cybercrime baik terkait UU ITE, dan segala bentuk kejahatan yang dilakukan melalui teknologi. ')}} 
                    </p>
                    <div><span class="title-text">{{__('Tindak Pidana Dibawah Umur') }}</span></div>
                    <p class="body-text">
                        {{__('Para praktisi kami menangani perkara Tindak Pidana di bawah umur yaitu mencakup anak yang umurnya dibawah usia 18 tahun dengan pengklasifikasian menurut undang-undang yang harus memperhatikan kepentingan anak, akan tetapi selain dalam membela pelaku tindak pidana yang dilakukan dibawah umur para praktisi hukum kami juga menangani dari pihak korban yang mana pada praktiknya masih banyaknya korban dari hasil tindak pidana yaitu anak di bawah umur seperti pencabulan, asusila dsb. ')}}
                    </p>
                </div>
                <div class="col-sm-4">
                    <div><span class="title-text">{{__('Pembunuhan')}}</span></div>
                    <p class="body-text">
                        {{__('Para praktisi hukum kami telah menuntaskan berbagai perkara dalam bidang Pembunuhan dengan melakukan pendampingan di kepolisian dan pembelaan di tingkat pengadilan untuk memperjelas kronologi yang disangkakan karena di dalam kasus pembunuhan adanya unsur kesengajaan dan tidak sengaja atau membela diri yang di mana itu bisa mempengaruhi vonis dalam pertimbangan hakim dalam menjatuhkan hukuman berdasarkan terbuktinya niat pelaku, terbuktinya perbuatan, kebenaran dan kelengkapan dari alat bukti serta kesaksian dari para saksi yang dapat dipertanggungjawabkan. ')}}
                    </p>
                    <div><span class="title-text">{{__('Tindak Pidana Korporasi')}}</span></div>
                    <p class="body-text">
                        {{__('Tindak Pidana Korporasi adalah kejahatan-kejahatan perusahaan yang dilakukan oleh pengusaha ataupun karyawan di dalam suatu perusahaan atau dalam berbagai perusahaan. seperti melakukan tindakan Fraud, penyalahgunaan wewenang, penggelapan dalam jabatan, menyalahgunakan fasilitas dan sebagainya sehingga merugikan masyarakat, seperti iklan yang menyesatkan, pencemaran lingkungan, eksploitasi terhadap kaum pekerja/ buruh, manipulasi restitusi pajak, manipulasi dana masyarakat, sehingga Praktisi hukum kami dapat membantu anda dalam menangani berbagai perkara tindak pidana korporasi untuk membela hak hak klien sebagaimana yang diatur oleh Undang-Undang yang berlaku.')}}
                    </p>
                    <div><span class="title-text">{{__('Sengketa Pemilu')}}</span></div>
                    <p class="body-text">
                        {{__('Sengketa Pemilu adalah sengketa yg terjadi antar Peserta Pemilu & sengketa Peserta Pemilu dengan Penyelenggara Pemilu sebagai akibat dikeluarkannya keputusan KPU, KPU Provinsi, KPU Kab/Kota. Praktisi Hukum kami telah berpengalaman dalam mengajukan perkara sengketa proses pemilu dan sengketa hasil pemilu apabila Dalam hal terjadi sengketa hasil pemilu, maka lembaga yang berwenang menyelesaikannya adalah Mahkamah Konstitusi (MK). Tetapi, untuk sengketa proses pemilu, lembaga yang berwenang untuk menerima, memeriksa, dan memutus penyelesaian sengketa proses tersebut adalah Badan Pengawas Pemilu (Bawaslu) dan Pengadilan Tata Usaha Negara (PTUN). Pengajuan gugatan atas sengketa proses pemilu ke PTUN dilakukan setelah upaya administrasi di Bawaslu telah digunakan. ')}}
                    </p>
                    <div><span class="title-text">{{__('Sengketa Tanah dan Pembebasan Lahan')}}</span></div>
                    <p class="body-text">
                        {{__('Sengketa tanah atau yang biasa dikatakan sengketa adalah perselisihan pertanahan antara orang perseorangan, badan hukum, atau lembaga yang tidak berdampak luas secara sosio-politis sedangkan pembebasan lahan adalah pencabutan hak atas kepemilikan tanah oleh pemerintah secara paksa yang dimiliki pihak lain untuk penyelenggaraan kepentingan umum. Para Praktisi Hukum kami sudah berpengalaman dan profesional dalam menangani perkara tersebut yang tentunya menimbang akan kebenaran, kekuatan dokumen dan kemenangan kasus yang kita berikan dengan advice terbaik kepada Klien.')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    

@endsection 

