@extends('templates.main')
@section('content')
        <style>
        .body-text {
            text-align: justify;
            font-size: 9.8px;
            font-weight: 700
        }  
        .title-text {
            color: #b45f06;
            font-size: 10.8px
        }  
        ul {
            padding-left: 11px;
        }
    </style>  
    <div class="row" style="margin-right: 0;"> 
        <div class="col-lg-12" style="padding-right: 0">
                <div class="masthead" id="page5-6" style="padding-top: 6.5rem">
                    <div class="container" style="color: white;">
                        <div class="col-lg-12" id="page3-title">
                            <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                            <span style="font-size: 20px"><b>{{__('PRAKTISI & PORTOFOLIO TIM')}}</b></span><br><br>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="row" style="margin-right: 0; padding: 25px;">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/Matheace_Ramaputra.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                            <span class="title-text"><b>Matheace Ramaputra,S.H. - <i>PARTNER</i></b></span><br>
                            {{__('Matheace mengawali karir hukumnya di salah satu kantor advokat, dan di usia 25 tahun dia telah dipercaya untuk menjadi co-leader dalam salah satu proyek pembangkit listrik berskala nasional. Lalu Matheace melanjutkan karir di kantor advokat tertua di Indonesia untuk beberapa proyek di bidang merger & akuisisi, infrastruktur, dan perhutanan. Pada tahun 2018, Matheace tergabung sebagai konsultan di beberapa perusahaan start-up dan memimpin beberapa proyek fundraising dan Strategi Nasional Kecerdasan Buatan, serta terlibat di beberapa proyek pemerintah seperti National Capital Integrated Coastal Defense, LRT Jakarta Initiatives, Ibu Kota Baru.')}} 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <p class="title-text text-black"><b>{{__('Pengalaman:')}}</b></p>
                    <div class="body-text">
                        <p>
                            <ul>
                                <li>{{__('Fundraising Bridge Round USD 2,5 Juta Dollar mewakili Kata.ai, memimpin proyek dalam drafting, advising, dan negosiasi dengan pihak Venture Capital TPTF.')}}</li>
                                <li>{{__('Memimpin proyek pendanaan untuk 10 Film Indonesia yang mewakili Venture Capital Ideosources.')}}</li>
                                <li>{{__('Memimpin proyek app-to-app agreement dengan mewakili Migo dengan perusahaan multinasional untuk sharing content film dan series di aplikasi SVOD (Subscription Video On Demand).')}}</li>
                                <li>{{__('Memimpin Fundraising Series B bagi startup Kata.ai, termasuk dalam drafting dan negosiasi dengan pihak Venture Capital TPTF, MDI, dan Artha Prima Financel.')}}</li>
                                <li>{{__('Asistensi dalam proyek akuisisi perusahaan yang dilakukan oleh Baramulti Group yang bergerak di bidang pengelolaan hutan produksi.')}}</li>
                                <li>{{__('Asistensi dalam kajian hukum untuk PT. PLN (Persero) dan uji tuntas dalam 34 proyek pembangkit listrik yang bermasalah.')}}</li>
                                <li>{{__('Asistensi dengan Bappenas dalam proyek kajian hukum penataan dan perencanaan reklamasi Teluk Jakarta. ')}}</li>
                                <li>{{__('Asistensi dengan Bappenas dalam pembuatan undang-undang Ibu Kota Negara Republik Indonesia.')}}</li>
                                <li>{{__('Asistensi dengan Pemprov DKI Jakarta dalam kajian hukum proyek LRT Jakarta.')}}</li>
                                <li>{{__('Asistensi PT. Archi Indonesia dalam persiapan kepatuhan hukum bagi perusahaan tambang dalam rencana melaksanakan IPO.')}}</li>
                                <li>{{__('Asistensi PT. PLN (Persero) dalam proyek pembangkit listrik kapal (vessel power plant).')}}</li>
                                <li>{{__('Memimpin proyek pendanaan Financing Arrangement senilai USD 10 juta Dollar untuk perusahaan tambang batu bara PT. Dua Bersaudara.')}}</li>
                                <li>{{__('Asistensi PT. HP Indonesia dalam proses klaim di Singapore International Arbitration Centre (SIAC) dalam kasus chip delivery agreement.')}}</li>
                                <li>{{__('Asistensi BPPT terhadap Strategi Nasional Kecerdasan Artifisial.')}}</li>
                                <li>{{__('Memimpin proyek pendanaan Migo dalam Series C dengan MNC Group.')}}</li>
                                <li>{{__('Asistensi BOP Labuan Bajo dalam kajian hukum dan contractual arrangement dalam proyek mega wisata Mandalika Moto GP.')}}</li>
                                <li>{{__('Asistensi kajian hukum, risiko, dan contractual arrangement untuk Perumda Pembangunan Sarana Jaya dalam proyek ITF penugasan Proyek Strategis Nasional DKI Jakarta.')}}</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/M_Alif_Alatas.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                        <span class="title-text"><b>M. Alif Alatas, S.H. - <i>PARTNER</i></b></span><br>
                        {{__('Alif mengawali karir di dunia hukum sebagai litigator di salah satu Lembaga Bantuan Hukum ternama yang memiliki pengalaman yang luar biasa dalam penyelesaian perkara-perkara litigasi non komersial atau secara pro bono, pencapaian penanganan rata-rata 50 kasus dalam satu bulan. Alif memiliki pengalaman dan pengetahuan yang luar biasa dalam menyelesaikan banyak perkara litigasi baik dalam ranah pidana, Perselisihan Hubungan Industrial, Perceraian, perdata komersial, dan tata usaha negara, Pengurusan Transportasi, Ekspor/Impor, Pergudangan, penanaman modal Dalam Negeri/PMA, pendirian perusahaan, serta perizinan usaha. ')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <p class="title-text text-black"><b>{{__('Pengalaman:')}}</b></p>
                    <div class="body-text">
                        <p>
                            <ul>
                                <li>{{__('Asistensi terhadap calon DPD RI Provinsi Banten dalam penanganan mediasi di KPU, perkara gugatan Pemilu di Pengadilan Tata Usaha Negara atas adanya dugaan kecurangan dalam pemilihan umum.')}}</li>
                                <li>{{__('Asistensi dalam perancangan kajian hukum terhadap Pemerintah Daerah Serang bekerjasama dengan Dinas Pekerjaan Umum Serang, dan Perguruan Tinggi sehubungan dengan perencanaan aturan tata kelola Dinas Lingkungan Hidup.')}}</li>
                                <li>{{__('Asistensi terhadap PT. Nissin Transport Indonesia dalam pengurusan pidana kasus penggelapan dalam jabatan, penipuan penyelesaian perkara tindak pidana dan perkara perdata komersial, termasuk namun tidak terbatas pada Proses Hukum di Kepolisian, Kejaksaan dan Pengadilan, Somasi dan gugatan dalam proses hukum Perdata serta penanganan aspek kepatuhan hukum, manajemen risiko perusahaan, perizinan, legalitas perusahaan, perselisihan ketenagakerjaan, pengurusan Izin TKA.')}}</li>
                                <li>{{__('Asistensi terhadap PT. Nissin Jaya Indonesia dalam pengurusan kasus hubungan industrial dari buruh dan pengusaha.')}}</li>
                                <li>{{__('Asistensi terhadap PT. APM Hyundai Transys dalam melakukan upaya hukum kepada salah satu karyawan yang melakukan tindak sewenang2 dalam jabatan yang melakukan perjanjian dengan pihak Vendor tanpa diketahui oleh pihak Direktur dan Komisaris.')}}</li>
                                <li>{{__('Asistensi terhadap Buruh melawan berbagai perusahaan menghadapi berbagai kasus dalam mengajukan gugatan kepada Pengadilan Hubungan Industrial. ')}}</li>
                                <li>{{__('Asistensi terhadap PT. Muia Niaga Prima dan PT. Mulia Jasa Prima dalam memberikan konsultasi pada proyek Konstruksi kepada pemerintah, konsultan dalam pengurusan izin dalam proyek, beserta aspek hukum yang harus ditaati dalam pelaksanaan proyek tersebut.')}}</li>
                                <li>{{__('Asistensi dalam pendirian perusahaan di bidang industri seperti dalam bidang Manufacture, Otomotif, outsourcing, konstruksi, ATK, ekspor/impor, pergudangan, Jasa Pengurusan Transportasi baik perizinan perusahaan dalam negeri maupun perusahaan asing. ')}}</li>
                                <li>{{__('Asistensi dalam pemberian nasihat hukum terhadap perusahaan pada perkara penggelapan dan penipuan, utang piutang, ketenagakerjaan, perizinan umum dan khusus dan lain sebagainya.')}}</li>
                                <li>{{__('Asistensi dalam memberikan layanan mediasi, negosiasi dan rekonsiliasi terhadap perkara ketenagakerjaan, sengketa pertanahan, tindak pidana umum/khusus, dan sebagainya.')}}</li>
                                <li>{{__('Asistensi dalam menangani perkara perceraian, gono-gini, hak asuh anak, penetapan, sengketa waris, dan sebagainya.')}}</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/Bryand_Ery_Sianipar.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                            <span class="title-text"><b>Bryand Ery Sianipar, S.H. - <i>BUSINESS PARTNER</i></b></span><br>
                            {{__('Bryand mengawali karirnya sebagai litigator di salah satu kantor hukum litigasi di Jakarta yang memiliki jaringan dan pengalaman yang luas dalam penyelesaian perkara-perkara litigasi di Indonesia, sehingga memberikan Bryand pengalaman dan pengetahuan yang luar biasa dalam menyelesaikan banyak perkara litigasi dalam ranah Perdata komersial dan ranah Pidana sehingga memberikan ia kecakapan di berbagai bidang yang diantaranya meliputi bidang hukum penanaman modal, pertanahan, penundaan pembayaran kewajiban utang, kepailitan dan bidang corporate litigasi lainnya. ')}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <p class="title-text text-black"><b>{{__('Pengalaman:')}}</b></p>
                    <div class="body-text">
                        <p>
                            <ul style="line-height: 2;">
                                <li>{{__('Asistensi terhadap PT. Graha Multi Pangan dalam perkara gugatan Wanprestasi di Pengadilan Negeri.')}}</li>
                                <li>{{__('Asistensi terhadap PT. Putra Mandiri Jembar dalam melakukan Initial Public Offering (IPO) menjadi PT. Putra Mandiri Jembar Tbk.')}}</li>
                                <li>{{__('Asistensi terhadap PT. Logomas Utama dalam memberikan konsultasi pada proyek Tambang Pasir Laut.')}}</li>
                                <li>{{__('Asistensi terhadap Afiliasi PT Perusahaan Gas Negara Tbk. dalam manajemen risiko perusahaan, perizinan dan kepatuhan hukum, legalitas perusahaan, perselisihan ketenagakerjaan serta penyelesaian perkara tindak pidana.')}}</li>
                                <li>{{__('Asistensi terhadap PT. Putra Mandiri Jembar Tbk. untuk mendampingi dan/atau Mewakili Perusahaan dalam Proses Hukum di Kepolisian, Kejaksaan dan Pengadilan, Memberikan opini-opini hukum terhadap isu Perusahaan')}}</li>
                                <li>{{__('Asistensi terhadap PT. Dipo Internasional Pahala Otomotif dalam aksi korporasi Merger Perusahaan.')}}</li>
                                <li>{{__('Asistensi terhadap PT. Sinar Mandiri Logistik dalam penanganan penyelesaian perkara wanprestasi.')}}</li>
                                <li>{{__('Asistensi terhadap DIPO Group (PT. Dipo Internasional Pahala Otomotif, PT. Dipo Angkasa Motor, PT. Mokas Otomotif Sejahtera) dalam menangani aspek kepatuhan, manajemen risiko perusahaan, perizinan dan kepatuhan hukum, legalitas perusahaan, perselisihan ketenagakerjaan serta penyelesaian perkara tindak pidana dan perkara perdata komersial, termasuk namun tidak terbatas pada Proses Hukum di Kepolisian, Kejaksaan dan Pengadilan.')}}</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection 

