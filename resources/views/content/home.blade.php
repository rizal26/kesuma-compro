@extends('templates.main')
@section('content')
        <!-- Mashead header-->
        <div class="masthead" id="page1" style="height: -webkit-fill-available;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <!-- Mashead text and app badges-->
                        <div style="color: white ; position: absolute; bottom: 0;">
                            <table>
                                <tr>
                                    <td>
                                        <img src="{{ asset('assets/images/logo.png') }}" id="home-logo" alt="" style="padding-bottom: 15px">
                                    </td>
                                    <td style="vertical-align: bottom;">
                                        <h1>
                                            <span class="initial">K</span>ESUMA <span class="initial">P</span>ARTNERS<br>
                                        </h1>   
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <h6 style="position: relative; top: -14px" ><i id="counselor" style="float: right; text-align: right">Counselors at Law</i></h6>
                                    </td>
                                </tr>
                            </table>
                            <div style="font-size: 12px">
                                <p>
                                Infiniti Office, Belleza BSA 1st Floor, Unit 106 <br>
                                Jl. Permata Hijau, RT.4/RW.2, Grogol Utara, <br>
                                Kec. Kby. Lama, Jakarta Selatan, DKI Jakarta <br>
                                P. 62 813 8346 3019 <br>
                                E. info@kesumapartner.com <br>
                                www.kesumapartner.com
                                </p>
                                <p>
                                Copyright of Kesuma Partners <br>
                                Company Profile - Version 2.1/{{ __('Bahasa Indonesia') }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection 

