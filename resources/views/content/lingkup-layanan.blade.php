@extends('templates.main')
@section('content')
        <style>
        .body-text {
            text-align: justify;
            font-size: 10px;
        }  
        .title-text {
            color: #b45f06;
            font-size: 14px;
        }  
    </style>  
    <div class="row" style="margin-right: 0; height: 100%"> 
        <div class="col-lg-3" style="padding-right: 0">
            <div class="masthead" style="padding-top: 5rem; background-color: white; color: white; height: 100%" id="page3" >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="position: fixed" id="page3-title">
                            <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                            <span><b>{{__('LINGKUP LAYANAN')}}</b></span><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9" style="background-color: #434343;">
            <div class="row" style="padding-top: 5rem; color: white;" id="page3-text">
                {{-- <div class="col-lg-3">
                </div> --}}
                <div class="col-sm-4">
                    <div><span class="title-text">{{__('Infrastruktur & Pembiayaan Proyek')}}</span></div>
                    <p class="body-text">
                        {{__('Keadaan ekonomi di Indonesia yang memiliki perkembangan pesat telah berhasil mengundang ketertarikan investor-investor asing untuk menyelenggarakan proyek infrastruktur di Indonesia, sebagai contoh, jalan tol dan moda transportasi massal seperti Mass Rapid Transportation. Praktisi hukum kami dapat memberikan asistensi serta mempersiapkan setiap dokumen transaksional yang dibutuhkan seperti perjanjian fasilitas pinjaman pembiayaan proyek, perjanjian gadai, perjanjian kerja sama Pemerintah dan Badan Usaha. Selain itu, dengan berbekal pemahaman dan praktik hukum yang ekstensif di berbagai lini kegiatan usaha, kami dapat memberikan asistensi di berbagai jenis proyek seperti transportasi, pembangkit listrik, dan jalan tol.')}}
                    </p>
                    <div><span class="title-text">{{__('Investasi')}}</span></div>
                    <p class="body-text">
                        {{__('Dengan pengalaman yang ekstensif dan variatif, praktisi hukum telah memiliki kompetensi yang sangat baik dalam melakukan asistensi kepada klien untuk melakukan kegiatan-kegiatan usaha dan investasi di Indonesia. Adapun, kami dapat memberikan asistensi dalam bentuk termasuk namun tidak terbatas pada, pendirian perseroan terbatas asing maupun modal dalam negeri serta bentuk badan usaha lainnya, perolehan izin usaha dan izin-izin terkait lainnya,dan memberikan nasihat terkait komposisi pemegang saham perseroan terbatas sesuai dengan peraturan yang berlaku di Indonesia.')}}
                    </p>
                    <div><span class="title-text">{{__('Keuangan & Perbankan')}}</span></div>
                    <p class="body-text">
                        {{__('Praktisi hukum kami berpengalaman dalam memberikan asistensi klien yang ingin memasuki pasar perbankan, pembiayaan dan asuransi di Indonesia dengan memberikan nasihat tentang semua aspek hukum dalam pendirian dan pengoperasian lembaga keuangan perbankan maupun non-bank di indonesia. Kami mampu memberikan asistensi dalam membuat perjanjian pinjaman dan dokumen jaminan untuk lembaga keuangan, baik di dalam maupun di luar indonesia, serta membantu investor untuk mendirikan dan mengoperasikan reksa dana dan program pensiun. Kami juga berfungsi sebagai penasihat dalam pemberi pinjaman asing pada pembiayaan proyek dan transaksi pinjaman komersial sindikasi dan restrukturisasi utang perusahaan')}}
                    </p>
                    <div><span class="title-text">{{__('Asuransi')}}</span></div>
                    <p class="body-text">
                        {{__('Praktisi Hukum kami telah berpengalaman dalam membantu investor untuk baik membangun usaha bidang perasuransian maupun menjaga kepatuhan pada peraturan penyelenggaraan perasuransian di Indonesia yang membutuhkan pengalaman dalam menilai keunikan setiap kasus serta membantu pemenuhan perizinan yang dibutuhkan.')}}
                    </p>
                    <p class="body-text">
                        {{__('Selain itu, kami juga dapat memberikan asistensi yang dibutuhkan dalam operasional suatu perusahaan perasuransian, yakni antara lain, menyediakan template perjanjian, penyusunan skema bisnis operasional, pendirian cabang, serta pendampingan litigasi terkait penyelesaian kasus dengan agen atau nasabah.')}}
                    </p>
                </div>
                <div class="col-sm-4">
                    <div><span class="title-text">{{__('Merger & Akuisisi')}}</span></div>
                    <p class="body-text">
                        {{__('Praktisi hukum kami berpengalaman dalam memberikan asistensi bagi perusahaan-perusahaan baik domestik, multinasional, maupun asing yang memiliki reputasi yang tinggi, dalam mempersiapkan segala bentuk dokumentasi maupun tindakan-tindakan yang diperlukan dalam transaksi merger & akuisisi. Selain pemberian nasehat hukum, adapun jenis-jenis dokumen yang dapat kami berikan adalah, antara lain, perjanjian pemegang saham, perjanjian gadai saham, daftar pemegang saham, dan perjanjian jual beli saham bersyarat, serta dokumen-dokumen esensial lainnya.')}}
                    </p>
                    <div><span class="title-text">{{__('Teknologi')}}</span></div>
                    <p class="body-text">
                        {{__('Para praktisi hukum Kesuma Partners sangat berpengalaman dalam melakukan asistensi kepada perusahaan-perusahaan yang melakukan kegiatan usaha di bidang teknologi di Indonesia, khususnya dalam penyediaan dokumen-dokumen untuk memenuhi kelengkapan produk ataupun dalam bertransaksi. Dokumen-dokumen yang kami sediakan diantaranya meliputi perancangan perjanjian berlangganan, syarat dan ketentuan produk, end user license agreement (EULA), kebijakan privasi, kebijakan cookies, kebijakan keamanan, kebijakan pengelolaan data, batasan tanggung jawab layanan, perjanjian lisensi produk, ketentuan pemeliharaan produk, perjanjian uji coba produk, perjanjian sandboxing, perjanjian integrasi produk, perjanjian riset dan pengembangan, service level agreement (SLA).')}}
                    </p>
                    <div><span class="title-text">{{__('Perkapalan')}}</span></div>
                    <p class="body-text">
                        {{__('Bidang usaha perkapalan memiliki keunikan tersendiri, baik dari segi lisensi, jenis perjanjian, otoritas terkait, serta jenis kasus yang terjadi. Oleh karenanya, diperlukan pengalaman dan pemahaman yang komprehensif atas praktik dan implementasi peraturan perundangan-perundangan terkait perkapalan yang ada di Indonesia. Praktisi hukum kami telah memiliki pengalaman, dalam uji tuntas perizinan perusahaan di bidang perkapalan serta dalam penyelesaian sengketa baik dari pihak pemilik kapal, pemilik barang, hingga pengangkut (freight forwarder). Kami juga menyiapkan perjanjian pengangkutan atau perjanjian time charter bagi klien kami. Selain itu kami juga memiliki pengetahuan khusus terkait sisi komersial dari bisnis perkapalan.')}} 
                    </p>
                    <div><span class="title-text">{{__('Oil & Gas')}}</span></div>
                    <p class="body-text">
                        {{__('Para praktisi Kesuma Partners telah banyak terlibat dalam berbagai transaksi eksplorasi dan eksploitasi migas, antara lain Perjanjian Kerja Sama Operasi, Kontrak Produksi, Perjanjian Teknis, Pengadaan Tanah, Perjanjian Rig Pengeboran, Perjanjian Produk dan Jasa, dan masih banyak lagi Perjanjian lainnya. ')}}
                    </p>
                    <p class="body-text">
                        {{__('Kami juga menjaga hubungan kami dengan regulator terkait serta perusahaan-perusahaan yang bergerak di industri yang sama agar kami dapat selalu ter-informasi mengenai perubahan-perubahan atas peraturan perundang-undangan dan peraturan-peraturan di sektor minyak dan gas.')}}
                    </p>
                </div>
                <div class="col-sm-4">
                    <div><span class="title-text">{{__('PKPU & Kepailitan')}}</span></div>
                    <p class="body-text">
                        {{__('Kesuma Partners menyediakan asistensi baik bagi perorangan maupun perusahaan dalam menyelesaikan permasalahan utang piutang yang mungkin tertunda atau belum dilaksanakan sama sekali oleh Debitur dengan cara mengajukan permohonan Penundaan Kewajiban Pembayaran Utang (PKPU) ataupun untuk mengajukan permohonan Pailit terhadap Debitur ke pengadilan Niaga agar dapat melindungi kepentingan klien-klien kami, karena kami sangat memahami bahwa pelaksanaan pembayaran merupakan hal yang sangat vital bagi klien-klien kami dalam menjalankan kegiatan usahanya.')}}
                    </p>
                    <div><span class="title-text">{{__('Restrukturisasi Utang dan Insolvensi')}}</span></div>
                    <p class="body-text">
                        {{__('Utang-piutang adalah salah satu kegiatan yang umum dilakukan oleh perusahaan dan merupakan hal yang sangat berdampak pada laju bisnis perusahaan. Oleh karenanya, praktisi hukum kami menyediakan asistensi atas kegiatan hukum yang berkaitan dengan restrukturisasi utang dan insolvensi.')}}
                    </p>
                    <p class="body-text">
                        {{__('Adapun asistensi dan tindakan-tindakan yang dimaksud adalah termasuk namun tidak terbatas pada, melakukan negosiasi penyelesaian utang, membuat perjanjian restrukturisasi utang, membuat perjanjian perdamaian, perjanjian pengalihan utang ataupun perjanjian penghapusan utang.')}}
                    </p>
                    <div><span class="title-text">{{__('Perolehan Hak Akses Dukcapil')}}</span></div>
                    <p class="body-text">
                        {{__('Dalam bidang usaha tertentu, proses pengenalan nasabah atau Know Your Customer (“KYC”) untuk mencegah adanya penipuan dan tindak pidana lainnya, khususnya di kegiatan usaha terkait penyelenggaraan jasa keuangan perbankan maupun non-bank. Salah satu upaya konkrit untuk mengimplementasikan KYC adalah melakukan pengumpulan data pribadi dari setiap nasabah. Pada akhir tahun 2019, Direktorat Jenderal Kependudukan dan Catatan Sipil Republik Indonesia menerbitkan peraturan yang memperbolehkan institusi maupun badan usaha dengan bidang usaha apapun untuk memperoleh hak akses data kependudukan Indonesia dengan persyaratan tertentu. Praktisi Hukum kami telah berhasil memberikan asistensi kepada klien-klien kami dalam memperoleh hak akses tersebut.')}}
                    </p>
                    <div><span class="title-text">{{__('Persaingan Usaha')}}</span></div>
                    <p class="body-text">
                        {{__('Dengan berbekal pengalaman di berbagai perusahaan multinasional, Praktisi Hukum Kesuma Partners telah memiliki pengalaman di bidang persaingan usaha tidak sehat. Pengalaman para pengacara Kesuma Partners termasuk dan tidak terbatas pada penilaian resiko pelanggaran, pendampingan pemeriksaan dugaan pelanggaran (sejak tahap investigasi hingga peradilan), asistensi kepatuhan pelaporan kepada Komisi Persaingan Usaha Tidak Sehat dan sebagainya.')}}
                    </p>
                    <p class="body-text">
                        {{__('Pada layanan pencegahan, Kesuma Partners akan mempelajari project business development klien dan memberikan penilaian resiko serta memberikan nasihat hukum agar tidak terjadi pelanggaran.')}} 
                    </p>
                </div>
            </div>
        </div>
    </div>
    

@endsection 

