@extends('templates.main')
@section('content')
        <style>
        .body-text {
            text-align: justify;
            font-size: 12px;
            color: white
        }  
        .title-text {
            color: #b45f06;
            font-size: 14px;
        }  
        .initial-end {
            font-size: 19px;
        }
        
    </style>  
    <div class="row" style="margin-right: 0; height: 100%"> 
        <div class="col-lg-7" style="padding-right: 0">
            <div class="masthead" style="padding-top: 5rem; background-color: white; color: white; height: 100%" id="page7" >
                <div class="container">
                    
                </div>
            </div>
        </div>
        <div class="col-lg-5" style="background-color: #434343;">
            <div class="container h-100">
                <div class="row" style="padding-top: 7rem" id="page7-text">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                        <span style="color: white"><b>{{__('Eksklusifitas Klien')}}</b></span><br><br>
                                
                        <p class="body-text">
                            {{__('Kami hanya mengambil pekerjaan yang sesuai dengan pengalaman, pengetahuan dan bidang keahlian kami. Ketekunan dan disiplin kami dalam menyelesaikan tugas secara tepat waktu serta pelaporan perkembangan pekerjaan secara berkala dengan penjelasan yang lengkap dan terbuka kepada klien, keramahan dan responsivitas atas setiap panggilan klien, merupakan kunci utama kami dalam meraih kepercayaan klien kami.')}}
                        </p>
                        <p class="body-text">
                            {{__('Koneksi dan/atau jaringan yang luas dengan Pemerintahan serta manajemen pengarsipan yang telah terkomputerisasi, memberikan kami keunggulan dari kantor hukum lainnya.')}}
                        </p>
                        <p class="body-text">
                            {{__('Harga yang dapat dinegosiasikan dan metode pembayaran yang variatif serta manfaat tambahan diluar tugas utama kami, memberikan klien kami kenyamanan dan kepuasan atas layanan yang kami sediakan.')}}
                        </p>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-lg-12" style="padding-right: 0">
                        <div style="color: white ;">
                            <table style="float: right">
                                <tr>
                                    <td>
                                        <img src="{{ asset('assets/images/logo.png') }}" id="end-logo" alt="" style="padding-bottom: 11px; width: 40px">
                                    </td>
                                    <td style="vertical-align: bottom;">
                                        <h3>
                                            K<span class="initial-end">ESUMA</span> P<span class="initial-end">ARTNERS</span><br>
                                        </h3>   
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <h6 style="position: relative; top: -14px" ><i style="float: right; text-align: right; font-size: 12px;">Counselors at Law</i></h6>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

@endsection 

