@extends('templates.main')
@section('content')
        <style>
        .body-text {
            text-align: justify;
            font-size: 10px;
        }  
        .title-text {
            color: #b45f06;
            font-size: 14px;
        }  
    </style>  
    <div class="row" style="margin-right: 0; height: 100%"> 
        <div class="col-lg-3" style="padding-right: 0">
            <div class="masthead" style="padding-top: 5rem; background-color: white; color: white; height: 100%" id="page4" >
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="position: fixed" id="page4-title">
                            <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                            <span><b>{{__('LINGKUP LAYANAN')}}</b></span><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9" style="background-color: #434343;">
            <div class="row" style="padding-top: 5rem; color: white;" id="page4-text">
                {{-- <div class="col-lg-3">
                </div> --}}
                <div class="col-sm-4">
                    <div><span class="title-text">{{__('Perpajakan')}}</span></div>
                    <p class="body-text">
                        {{__('Praktisi Kesuma Partners selalu berusaha untuk memberikan kinerja terbaik kepada klien kami dengan melakukan penelitian mendalam untuk menemukan pendekatan pajak yang paling ramah untuk diterapkan di semua jenis transaksi, dengan mengandalkan pengetahuan kami yang luas tentang peraturan perundang-undangan dan kebijakan pajak saat ini, serta dengan memanfaatkan hubungan baik kami secara profesional dengan Direktorat Jenderal Pajak. ')}}
                    </p>
                    <div><span class="title-text">{{__('Kepatuhan & Manajemen Resiko')}}</span></div>
                    <p class="body-text">
                        {{__('Kami menyediakan layanan dalam bentuk uji tuntas atas kepatuhan perusahaan dalam menjalankan kegiatan usahanya berdasarkan peraturan perundang-undangan ataupun atas aturan dari otoritas terkait, untuk kemudian kami dapat memberikan serangkaian rekomendasi-rekomendasi yang dapat dijadikan acuan bagi perusahaan.')}}
                    </p>
                    <p class="body-text">
                        {{__('tidak hanya itu, kami juga dapat membantu klien kami untuk menganalisa sertifikasi-sertifikasi yang diperlukan serta membantu klien dalam perolehannya.')}}
                    </p>
                    <div><span class="title-text">{{__('Real Estate')}}</span></div>
                    <p class="body-text">
                        {{__('Kami memahami bahwa pentingnya kejelasan atas kepemilikan aset berupa tanah dan/atau bangunan serta bentuk properti lainnya merupakan hal yang sangat vital dalam melakukan kegiatan usaha. ')}}
                    </p>
                    <p class="body-text">
                        {{__('Praktisi hukum kami telah banyak terlibat dalam melakukan asistensi terkait dengan real estat, anta lain, melakukan uji tuntas terhadap bidang-bidang tanah dengan luas ribuan hektar, perjanjian sewa menyewa dan/atau jual beli properti, memberikan nasihat terkait skema kepemilikan properti bagi pihak asing di Indonesia, serta pengakuisisian tanah dan/atau bangunan. ')}}
                    </p>
                    <div><span class="title-text">{{__('Arbitrase dan Penyelesaian Sengketa')}}</span></div>
                    <p class="body-text">
                        {{__('Sifat penyelesaian perkara di Pengadilan umumnya bersifat terbuka untuk umum sehingga memungkinkan adanya publisitas informasi yang bersifat confidential. Oleh karena itu seringkali Para Pihak di dalam suatu perjanjian komersial memilih penyelesaian sengketa melalui Badan Arbitrase. Praktisi hukum kami telah berpengalaman dalam mendampingi penyelesaian sengketa melalui Badan Arbitrase di Indonesia.')}}
                    </p>
                    <p class="body-text">
                        {{__('Selain itu kami juga melayani penyelesaian sengketa diluar pengadilan seperti melakukan negosiasi, konsolidasi dan mediasi yang menghasilkan solusi penyelesaian yang menguntungkan.')}}
                    </p>
                </div>
                <div class="col-sm-4">
                    <div><span class="title-text">{{__('Ketenagakerjaan')}}</span></div>
                    <p class="body-text">
                        {{__('Para praktisi hukum kami telah menuntaskan puluhan perkara ketenagakerjaan di berbagai perusahaan di berbagai sektor industri. Tim kami akan membantu anda baik dalam perancangan dokumen-dokumen yang diantaranya penyediaan Perjanjian Kerja Waktu Tertentu, Perjanjian Kerja Waktu Tidak Tertentu, Perjanjian Jasa Tenaga Ahli, Perjanjian Perbantuan, Perancangan Peraturan Perusahaan, Perolehan izin kerja Tenaga Kerja Asing, Perancangan Perjanjian Kerja Bersama, Perancangan Kode Etik Karyawan, serta maupun mewakili anda selaku kuasa hukum dalam penyelesaian perselisihan hak karyawan, penyelesaian perselisihan kepentingan karyawan, ataupun pemutusan hubungan kerja.')}}
                    </p>
                    <div><span class="title-text">{{__('Izin Tenaga Kerja Asing')}}</span></div>
                    <p class="body-text">
                        {{__('Penggunaan tenaga kerja asing pada badan hukum di Indonesia merupakan salah satu bentuk konkrit dalam melakukan alih teknologi dan alih pengetahuan dalam operasional maupun mengembangkan kegiatan usahanya. Selain itu, keterbatasan kompetensi sumber daya juga menjadi dasar utama untuk merekrut tenaga kerja Asing.')}}
                    </p>
                    <p class="body-text">
                        {{__('Praktisi hukum kami telah memiliki pengalaman dalam melakukan pengurusan izin tenaga kerja Asing maupun izin tinggal tenaga kerja asing, antara lain ITAS dan ITAP serta dokumen terkait lainnya sehubungan dengan keberadaan warga negara asing yang berada di Indonesia.')}}
                    </p>
                    <div><span class="title-text">{{__('Pendirian dan Pengurusan Perusahaan')}}</span></div>
                    <p class="body-text">
                        {{__('Kesuma Partners telah banyak memberikan asistensi kepada beragam perusahaan dalam mendirikan perusahaan, pengurusan izin perusahaan, pengurusan sertifikasi perusahaan, pendirian cabang perusahaan, pendirian anak perusahaan, perubahan struktur perusahaan, rapat umum pemegang saham tahunan dan rapat umum pemegang saham luar biasa.')}} 
                    </p>
                    <p class="body-text">
                        {{__('Serta menyediakan rancangan dokumen kebutuhan perusahaan seperti perjanjian para pendiri perusahaan, perjanjian para pemegang saham, perjanjian keterlibatan saham, perjanjian operasi gabungan, perjanjian perusahaan patungan, dan lain sebagainya.')}}
                    </p>
                    <div><span class="title-text">Healthcare</span></div>
                    <p class="body-text">
                        {{__('Kegiatan usaha di bidang kesehatan semakin berkembang di Indonesia. Kegiatan usaha dalam sektor kesehatan juga kian berkembang seperti penyedia aplikasi sitem informasi managemen rumah sakit dan layanan aplikasi kesehatan yang mendukung perkembangan bisnis bagi perusahaan yang bergerak di bidang kesehatan.')}}
                    </p>
                    <p class="body-text">
                        {{__('Praktisi hukum kami memiliki pengalaman dalam memberikan nasihat hukum, pengurusan lisensi, pembuatan aplikasi hingga perlindungan data. Selain itu kami juga berpengalaman dalam melakukan uji tuntas perizinan terkait lingkungan hidup bagi klinik dan rumah sakit di Indonesia.')}} 
                    </p>
                </div>
                <div class="col-sm-4">
                    <div><span class="title-text">{{__('Penyediaan Dokumen Transaksi Umum')}}</span></div>
                    <p class="body-text">
                        {{__('Kami telah banyak membantu perusahaan-perusahaan di Indonesia dengan cara menyediakan layanan dalam bentuk perancangan dokumen-dokumen komersial bagi klien-klien kami dalam menjalankan serta menunjang kegiatan usahanya yang antara lain meliputi penyediaan perjanjian kerja sama umum dan khusus, perjanjian jual beli, perjanjian pemasaran, perjanjian distributor/ distribusi, perjanjian reseller/pengecer, nota kesepahaman, perjanjian berlangganan, perjanjian sewa, perjanjian gadai, perjanjian fidusia, perjanjian lisensi, syarat & ketentuan, perjanjian agen, perjanjian waralaba dan lain sebagainya')}}
                    </p>
                    <div><span class="title-text">{{__('Sertifikasi Produk')}}</span></div>
                    <p class="body-text">
                        {{__('Selain perizinan usaha, terdapat sertifikasi-sertifikasi lain yang wajib diperoleh sebelum menyediakan layanan dan/atau produk oleh pelaku usaha kepada konsumennya maupun sebagai penunjang kualitas dari layanan dan/atau produk miliknya. Praktisi hukum kami memiliki pengalaman untuk memperoleh sertifikasi t pada layanan dan/atau produk yang beragam sehingga dapat memberikan asistensi anda dalam perolehan sertifikat-sertifikat tersebut.')}}
                    </p>
                    <p class="body-text">
                        {{__('Adapun, sertifikat-sertifikat dimaksud adalah, antara lain sebagai berikut: ISO 37001, OHSAS, SNI, Izin Edar, sertifikat Halal MUI, Ahli K3, SIUPMSE, PSE, BPOM, dan POSTEL.')}}
                    </p>
                    <div><span class="title-text">{{__('Hak Kekayaan Intelektual')}}</span></div>
                    <p class="body-text">
                        {{__('Praktisi hukum kami sangat menyadari pentingnya perlindungan Hak Kekayaan Intelektual (“HAKI”) dalam melakukan kegiatan usaha di Indonesia. ')}}
                    </p>
                    <p class="body-text">
                        {{__('Oleh karena itu, kami dapat membantu klien-klien kami dengan memberikan nasihat serta melakukan tindakan yang diperlukan untuk melindungi HAKI, antara lain, asistensi pendaftaran Hak Cipta, Hak Merek, Hak Paten, Desain Industri, desain tata letak ruang, perlindungan varietas tanaman, pembuatan perjanjian lisensi penggunaan Merek serta logo, dan perjanjian waralaba (franchise). ')}}
                    </p>
                    <div><span class="title-text">{{__('Penyediaan Tenaga Perbantuan')}}</span></div>
                    <p class="body-text">
                        {{__('Kesuma Partners menyediakan layanan dalam bentuk penyediaan tenaga perbantuan di bidang hukum (of counsel legal) untuk dapat ditempatkan di perusahaan anda dalam periode waktu tertentu berdasarkan kesepakatan.')}}
                    </p>
                    <p class="body-text">
                        {{__('Tentunya tenaga perbantuan yang kami sediakan adalah orang-orang terpilih yang telah mengikuti serangkaian pelatihan-pelatihan dari kami secara terus menerus, sehingga ia dapat berkontribusi secara maksimal saat bekerja di perusahaan anda, serta mampu untuk mencapai ekspektasi perusahaan anda.')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    

@endsection 

