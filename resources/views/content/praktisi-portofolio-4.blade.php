@extends('templates.main')
@section('content')
        <style>
        .body-text {
            text-align: justify;
            font-size: 9.8px;
            font-weight: 700
        }  
        .title-text {
            color: #b45f06;
            font-size: 10.8px
        }  
        ul {
            padding-left: 11px;
        }
    </style>  
    <div class="row" style="margin-right: 0;"> 
        <div class="col-lg-12" style="padding-right: 0">
                <div class="masthead" id="page5-6" style="padding-top: 6.5rem">
                    <div class="container" style="color: white;">
                        <div class="col-lg-12" id="page3-title">
                            <img src="{{ asset('assets/images/line.png') }}" alt="" width="80"><br>
                            <span style="font-size: 20px"><b>{{__('PRAKTISI & PORTOFOLIO TIM')}}</b></span><br><br>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="row" style="margin-right: 0; padding: 25px;">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/Ray_Baskoro.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                            <span class="title-text"><b>Ray Baskoro, S.H. - <i>SENIOR ASSOCIATE</i></b></span><br>
                            <p style="line-height: 2;">{{__('Setelah berhasil memperoleh gelar Sarjana Hukum dari Universitas Indonesia, mengawali karirnya di kantor hukum terbesar di Jakarta yang memiliki afiliasi dan jaringan global pada divisi litigasi (dispute resolution) dan telah banyak berinteraksi dengan klien-klien mancanegara. Ray memiliki pengalaman dalam berlitigasi dalam ranah perdata, tata usaha negara, hubungan industrial dan arbitrase, serta mendalami ekspertis bidang hukum ketenagakerjaan, merger dan akuisisi perusahaan, informasi teknologi, jasa keuangan non-bank, hukum perdagangan, pendirian dan perolehan perizinan perusahaan.')}} 
                            </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="body-text">
                        <p>
                            <ul style="line-height: 2;">
                                <li>{{__('Uji tuntas kode produk yang dipasarkan di Indonesia oleh PT Decathlon Sports Indonesia;')}}</li>
                                <li>{{__('Asistensi untuk memenuhi kepatuhan terhadap peraturan perundang-undangan yang berlaku, termasuk namun tidak terbatas pada, perizinan dan tindakan-tindakan korporasi, kepada PT Decathlon Sports Indonesia;')}}</li>
                                <li>{{__('Uji tuntas terhadap perusahaan tambang nikel yang berlokasi pada Provinsi Maluku Utara dalam rangka akuisisi oleh perusahaan asal Tiongkok;')}}</li>
                                <li>{{__('Uji tuntas terhadap perusahaan tambang panas bumi yang berlokasi di Provinsi Nusa Tenggara Timur dalam rangka akuisisi oleh perusahaan asal Tiongkok;')}}</li>
                                <li>{{__('Asistensi perolehan izin tenaga kerja asing PT Eisai Indonesia;')}}</li>
                                <li>{{__('Asistensi perolehan izin tenaga kerja asing PT Trafigura Indonesia;')}}</li>
                                <li>{{__('Asistensi perolehan izin Penyelenggara Sertifikat Elektronik serta izin-izin terkait untuk melakukan kegiatan usaha di Indonesia bagi PT Amazon Web Service Indonesia;')}}</li>
                                <li>{{__('Asistensi uji tuntas terhadap salah satu sekolah internasional di Indonesia dalam rangka akuisisi tanah yang dilakukan oleh Perusahaan Modal Asing di Indonesia;')}}</li>
                                <li>{{__('Uji tuntas terhadap bidang-bidang tanah yang berlokasi di Pasar Kemis, Provinsi Banten, seluas 26.000 Ha dalam rangka akuisisi tanah yang dilakukan oleh PT CFLD Indonesia;')}}</li>
                                <li>{{__('Asistensi perolehan sertifikat hak guna bangunan terhadap bidang-bidang tanah dengan total luas sebesar 48.000 Meter persegi yang berlokasi di Kabupaten Lombok Timur, Nusa Tenggara Barat, yang dilakukan oleh Perusahaan Modal Asing yang bergerak di bidang perhotelan;')}}</li>
                                <li>{{__('Penanganan perkara perdata antara PT Alam Bukit Tigapuluh (anak perusahaan dari Yayasan WWF Indonesia) melawan direkturnya, sehubungan dengan pemecatan dirinya sebagai direktur PT Alam Bukit Tigapuluh;')}}</li>
                                <li>{{__('Asistensi perolehan izin kerja dari Tenaga Kerja Asing;')}}</li>
                                <li>{{__('Uji tuntas terhadap PT Bank BTPN Tbk, sehubungan dengan penerbitan obligasi dengan skema Penawaran Umum Berkelanjutan;')}}</li>
                                <li>{{__('Asistensi dalam melakukan penyusunan kontrak kerja bagi Perusahaan Multinasional yang ada di Indonesia;')}}</li>
                                <li>{{__('Asistensi untuk kepatuhan dalam perlindungan data pribadi bagi beberapa perusahaan-perusahaan di Indonesia')}}</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-right: 0; padding: 25px; padding-top: 0 !important;">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ asset('assets/images/Mega_Carerra.png') }}" style="float:left;padding:1% 2% 0% 0%; width: auto; max-width: 33%">
                    <div class="body-text">
                            <span class="title-text"><b>Mega Carerra, S.H. - <i>SENIOR ASSOCIATE</i></b></span><br>
                            <p style="line-height: 2;">{{__('Mega mengawali karir sebagai legal professional sebagai associate lawyer di beberapa firma hukum di Indonesia. Lalu, setelah hampir 3 tahun bekerja di firma hukum, Mega memutuskan untuk mengembangkan kemampuannya sebagai legal professional dengan menjadi in-house counsel di perusahaan startup yang bergerak di bidang financial technology. Mega juga aktif menjadi freelance legal consultant untuk beberapa perusahaan startup yang bergerak di bidang e-commerce, healthtech, dan insurtech.')}} 
                            </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="body-text">
                        <p>
                            <ul style="line-height: 2;">
                                <li>{{__('Asistensi pelaksanaan bisnis operasional beberapa perusahaan peer to peer lending di Indonesia')}}</li>
                                <li>{{__('Asistensi pelaksanaan bisnis operasional perusahaan penyelenggara sertifikat elektronik di Indonesia')}}</li>
                                <li>{{__('Asistensi pelaksanaan beberapa penawaran umum berkelanjutan atas surat utang beberapa perusahaan lokal terbuka dan tertutup di Indonesia')}}</li>
                                <li>{{__('Asistensi uji tuntas pelaksanaan restrukturisasi perusahaan milik negara yang bergerak di bidang perkebunan')}}</li>
                                <li>{{__('Asistensi pelaksanaan bisnis operasional perusahaan penyedia jasa di bidang teknologi finansial untuk pelaksanaan open API di Indonesia')}}</li>
                                <li>{{__('Asistensi uji tuntas pelaksanaan pinjaman kepada salah satu perusahaan media penyiaran')}}</li>
                                <li>{{__('Asistensi penyusunan code of conduct sehubungan dengan perlindungan data pribadi konsumen untuk salah satu asosiasi perusahaan di Indonesia')}}</li>
                                <li>{{__('Asistensi pelaksanaan bisnis operasional untuk perusahaan yang bergerak di bidang health-tech dan insurtech')}}</li>
                                <li>{{__('Asistensi uji tuntas pelaksanaan transaksi akuisisi perusahaan terbuka yang bergerak di bidang perdagangan distribusi bahan bangunan')}}</li>
                                <li>{{__('Asistensi notifikasi pasca transaksi akuisisi kepada Komisi Pengawasan Persaingan Usaha untuk beberapa perusahaan lokal dan asing')}}</li>
                                <li>{{__('Asistensi penanganan perkara persaingan usaha untuk perusahaan yang bergerak di bidang FMCG - produk air mineral dalam kemasan')}}</li>
                            </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection 

