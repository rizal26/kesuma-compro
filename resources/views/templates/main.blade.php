<!DOCTYPE html>
<html lang="en">
    <head>
         @include('templates.header')
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top shadow-sm" id="mainNav">
            @include('templates.menu')
        </nav>
        @yield('content')
        @include('templates.js')
    </body>
</html>
