            <div class="container">
                <a class="navbar-brand fw-bold" href="/">
                    <img src="{{ asset('assets/images/logo-K.png') }}" alt="" style="width: 32px; position: fixed; top: 3px;">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="bi-list"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto me-4 my-3 my-lg-0">
                        <li class="nav-item"><a class="nav-link me-lg-3" href="{{ url('/') }}" id="home">{{__('Beranda')}}</a></li>
                        <li class="nav-item">
                            <a class="nav-link me-lg-3" href="{{ url('/ringkasan-eksekutif') }}" id="ringkasan-eksekutif">
                                {{__('Ringkasan Eksekutif')}}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" id="navbarDropdownMenuLink" class="nav-link me-lg-3 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                {{__('Lingkup Layanan')}}
                            </a>
                            <div class="dropdown-menu" id="dd-menu">
                                <a class="dropdown-item" href="{{ url('/lingkup-layanan') }}" id="lingkup-layanan">#1</a>
                                <a class="dropdown-item" href="{{ url('/lingkup-layanan-2') }}" id="lingkup-layanan-2">#2</a>
                                <a class="dropdown-item" href="{{ url('/lingkup-layanan-3') }}" id="lingkup-layanan-3">#3</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="#" id="navbarDropdownMenuLink2" class="nav-link me-lg-3 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                {{__('Praktisi & Portofolio Tim')}} 
                            </a>
                            <div class="dropdown-menu" id="dd-menu-2">
                                <a class="dropdown-item" href="{{ url('/praktisi-portofolio') }}" id="praktisi-portofolio">#1</a>
                                <a class="dropdown-item" href="{{ url('/praktisi-portofolio-2') }}" id="praktisi-portofolio-2">#2</a>
                                <a class="dropdown-item" href="{{ url('/praktisi-portofolio-3') }}" id="praktisi-portofolio-3">#3</a>
                                <a class="dropdown-item" href="{{ url('/praktisi-portofolio-4') }}" id="praktisi-portofolio-4">#4</a>
                            </div>
                        </li>
                        {{-- <li class="nav-item"><a class="nav-link me-lg-3" href="{{ url('/praktisi-portofolio') }}" id="praktisi-portofolio">Praktisi & Portofolio Tim</a></li> --}}
                        <li class="nav-item"><a class="nav-link me-lg-3" href="{{ url('/eksklusifitas-klien') }}" id="eksklusifitas-klien">{{__("Eksklusifitas Klien")}}</a></li>
                    </ul>
                    @foreach (config('app.available_locales') as $locale)
                        <a href="{{ route('locale', $locale) }}" class="lang"
                            @if (app()->getLocale() == $locale) style="font-weight: bold; text-decoration: underline" @endif>
                            <?php if ($locale == 'id') {
                                $locales = 'ID';
                            } else {
                                $locales = 'EN ';
                            } ?>
                        {{ $locales }}
                        </a> 
                        &nbsp;
                    @endforeach
                </div>
            </div>