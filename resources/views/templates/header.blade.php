        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Kesuma Partners</title>
        <link rel="icon" type="image/x-icon" href="{{asset('assets/favicon.ico')}}" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Google fonts-->
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&amp;display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&amp;display=swap" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{asset('assets/css/styles.css')}}" rel="stylesheet" />
        <style>
            body, html {
                height: 100%;
            }
            #page1 {
                background: url(assets/images/page1.jfif) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            #page2 {
                background: url(assets/images/page2.jpg) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            #page3 {
                background: url(assets/images/page3.jpg) no-repeat fixed; 
                -webkit-background-size: contain;
                -moz-background-size: contain;
                -o-background-size: contain;
                background-size: contain;
                /* box-shadow: inset 0 0 0 2000px rgb(0 0 0 / 50%); */
            }
            #page4 {
                background: url(assets/images/page4.png) no-repeat fixed; 
                -webkit-background-size: contain;
                -moz-background-size: contain;
                -o-background-size: contain;
                background-size: contain;
                /* box-shadow: inset 0 0 0 2000px rgb(0 0 0 / 50%); */
            }
            #page7 {
                background: url(assets/images/page7.jpg) no-repeat fixed; 
                -webkit-background-size: contain;
                -moz-background-size: contain;
                -o-background-size: contain;
                background-size: contain;
                /* box-shadow: inset 0 0 0 2000px rgb(0 0 0 / 50%); */
            }
            #page5-6 {
                background: url(assets/images/page5-6.jpg) no-repeat; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                /* box-shadow: inset 0 0 0 2000px rgb(0 0 0 / 50%); */
            }
            #page2:before {
                content: "";
                position: fixed;
                left: 49%; right: 25.2%;
                top: 0;
                bottom: 0;
                height: 100%;
                background: rgba(0,0,0,.5);
            }
            #mainNav {
                background-color: #000000b0;
            }
            @media (max-width: 990px) {
                #page2:before {
                    content: "";
                    position: fixed;
                    left: 0; right: 0;
                    top: 0;
                    bottom: 0;
                }
                #page-text2{
                    padding-top: 2rem !important;
                    padding-left: var(--bs-gutter-x, 0.75rem) !important;
                    background-color: white;
                    z-index: 999;
                }
                #page3-text, #page4-text, #page7-text {
                    padding: 1rem !important;
                }
                #page3-title, #page4-title {
                    position: unset !important;
                }
                #page4 {
                    background-size: cover;
                }
            }

        </style>