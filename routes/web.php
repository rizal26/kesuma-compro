<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Route::get('lang/{locale}', 'LocalizationController@switchLanguage')->name('locale');
Route::get('/', 'PageController@home');
Route::get('/ringkasan-eksekutif', 'PageController@ringkasanEksekutif');
Route::get('/lingkup-layanan', 'PageController@lingkupLayanan');
Route::get('/lingkup-layanan-2', 'PageController@lingkupLayananDua');
Route::get('/lingkup-layanan-3', 'PageController@lingkupLayananTiga');
Route::get('/praktisi-portofolio', 'PageController@praktisiPortofolio');
Route::get('/praktisi-portofolio-2', 'PageController@praktisiPortofolioDua');
Route::get('/praktisi-portofolio-3', 'PageController@praktisiPortofolioTiga');
Route::get('/praktisi-portofolio-4', 'PageController@praktisiPortofolioEmpat');
Route::get('/eksklusifitas-klien', 'PageController@eksklusifitasKlien');
